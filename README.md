# Wordpress using docker
a small wordpress development.
## requirements 
- docker
- docker-compose v3 or higher
## usage
- to run
``docker compose up -d``
- to delete 
``docker compose down -v``
Then delete the created floders.

**NOTE:** When on linux you must take back ownership of the files in the directory in order to delete them.